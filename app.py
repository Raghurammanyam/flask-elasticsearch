from flask import Flask
from flask_restful import Api
from flask_cors import CORS

app=Flask(__name__)
CORS(app,resources={r"/api/*":{"origins":"*"}})
api =Api(app)

from controller import post_index

api.add_resource(post_index,'/api/postindex')

from controller import search_Index,wildSearch

api.add_resource(search_Index,'/api/searchindex')

api.add_resource(wildSearch,'/api/searchphrase')


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')


