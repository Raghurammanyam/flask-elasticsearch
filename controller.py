from flask_restful import Resource
from flask import Flask, jsonify, request
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search,Q
from datetime import datetime

from time import clock
ES_HOST = 'http://localhost:9200'
es = Elasticsearch(ES_HOST)

search = Search(using=es,index='contents')


class post_index(Resource):
    def __init__(self):
        pass
    def post(self):
            slug = request.form['slug']
            title = request.form['title']
            content = request.form['content']

            body = {
                'slug': slug,
                'title': title,
                'content': content,
                'timestamp': datetime.now()
            }

            result = es.index(index='contents', doc_type='title', id=slug, body=body)

            return jsonify(result)
    def get(self):
        
         results = es.search(index='contents',body={"query": {"match_all": {}}, "size": 10000})
         return jsonify(results['hits']['hits'])
class search_Index(Resource):
    def __init__(self, *args, **kwargs):
        pass
    def post(self):
            keyword = request.form['keyword']

            body = {
                "query": {
                    "multi_match": {
                        "query": keyword,
                        "fields": ['contents','title','name','content','job']
                        
                    }
                }
            }

            res = es.search(index="contents", body=body)

            return jsonify(res['hits']['hits'])

class wildSearch(Resource):
    def __init__(self, *args, **kwargs):
        pass
    def post(self):
        keyword = request.form['keyword']
        print(type(keyword))


        body = {
            "query": {
                "match": {
                  
                    "name": keyword,
                  
                    
                }
            }
        }

        res = es.search(index="contents", body=body)

        return jsonify(res['hits']['hits'])

    def put(self):
        keyword =request.form['keyword']
        id=request.form['id']
        body = {
            
                "doc": {
                  
                    "name": keyword,
                  
                    
                }
            
        }

        response = es.update(index='contents', id=id, body=body)
        print ('response:', response)

        return jsonify(response)
    def delete(self):
        id =request.form['id']
        body={ 'query': { 'term': { 'name': id } } }
        response=es.delete(index="contents",id=id)
        # response = es.delete_by_query(index='contents',body= body)
        return jsonify(response)































#         curl -X GET "localhost:9200/_search?pretty" -H 'Content-Type: application/json' -d'
# {
#     "query": {
#         "wildcard": {
#             "user": {
#                 "value": "ki*y",
#                 "boost": 1.0,
#                 "rewrite": "constant_score"
#             }
#         }
#     }
# }
# '

        # res = es.search(body={"query":{"wildcard":{"user":{"value":keyword, "boost": 1.0,
        #         "rewrite": "constant_score"}}}},size=1000)
        # end=clock()
        # time_taken =end-start
        # print("%d documents found: "% res['hits']['total'],time_taken)
        # for doc in res['hits']['hits']:
        #     #print("%s) %s <mailto:%s> %s %s" % (doc['_id'], doc['_source']['name'],doc['_source']['email'],doc['_source']['job'],doc['_source']['fff']))
        #     print (doc)
        # return jsonify(res['hits']['hits'])
    

