import schedule
import subprocess
from datetime import datetime
import os
from time import clock
import time
backup = os.getcwd()


def main():
    time_strt1 = datetime.now()
    folder = backup+'/'+str(datetime.now())
    timestartfull = time()
    if not os.path.exists(folder):
        """ path doesn't exist. trying to make """
        os.makedirs(folder)

    subprocess.run(str(mysqlbackup - -incremental - -incremental-backup-dir=folder+'/' - -incremental-base=dir: / prod_ent_backup/2019-09-25_01-00-01 / ... backup))
    time_end1 = datetime.now()
    time1 = int(str(time_end1-time_strt1)[5:7])
    time.sleep(60-time1)
    time_strt2 = datetime.now()
    folder1files = os.listdir(folder)
    folder1 = backup+'/'+str(folder1files[0])
    if not os.path.exists(folder1):
        """ path doesn't exist. trying to make """
        os.makedirs(folder1)
    subprocess.run(str(mysqlbackup - -incremental - -incremental-backup-dir=folder1+'/' - -incremental-base=dir: / prod_ent_backup/2019-09-25_01-00-01 / ... backup))
    time_end2 = datetime.now()
    time2 = int(str(time_end2-time_strt2)[5:7])

    time.sleep(60-time2)
    time_strt3 = datetime.now()
    folder2files = os.listdir(folder1)
    folder2diff = list(set(folder2files)-set(folder1files))
    folder2 = backup+'/'+str(folder2diff[0])
    if not os.path.exists(folder2):
        """ path doesn't exist. trying to make """
        os.makedirs(folder2)
    subprocess.run(str(mysqlbackup - -incremental - -incremental-backup-dir=folder2+'/' - -incremental-base=dir: / prod_ent_backup/2019-09-25_01-00-01 / ... backup))
    time_end3 = datetime.now()
    time3 = int(str(time_end3-time_strt3)[5:7])

    time.sleep(60-time3)
    folder3files = os.listdir(folder2)

    folder3diff = list(set(folder3files)-set(folder2files))
    folder3 = backup+'/'+str(folder3diff[0])
    if not os.path.exists(folder3):
        """ path doesn't exist. trying to make """
        os.makedirs(folder3)
    subprocess.run(str(mysqlbackup - -incremental - -incremental-backup-dir=folder3+'/' - -incremental-base=dir: / prod_ent_backup/2019-09-25_01-00-01 / ... backup))


schedule.every(4).minutes.do(main)
while True:
    schedule.run_pending()
    time.sleep(1)
